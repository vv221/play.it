# ./play.it: Installer for DRM-free commercial games

The codebase is maintained at https://git.dotslashplay.it/scripts/

Bug reports and feature requests are tracked at https://forge.dotslashplay.it/play.it/play.it/-/issues

## Description

./play.it is a free software building native packages from installers for Windows or Linux, mainly those sold by stores focusing on DRM-free games distribution. The goal is that a game installed via ./play.it is indistinguishable from a game installed via the official repositories of your favourite distribution.

The games are installed globally on multi-user systems, avoiding unnecessary duplication. The locations of save games, settings, mods, temporary files and backups are standardized with XDG Base Directory support.

Packaging the games simplifies future updates, uninstalls and handling of any necessary dependencies, including integrated obsolete dependencies if specific versions are needed.

## Installation

### Distributions providing ./play.it

The following distributions provide installation instructions in their official documentation:

- [Debian]
- [Gentoo]
- [Ubuntu] (French article)

[Debian]: https://wiki.debian.org/Games/PlayIt#Installation
[Gentoo]: https://wiki.gentoo.org/wiki/Play.it#Installation
[Ubuntu]: https://doc.ubuntu-fr.org/play.it#installation

In most cases, these instructions should work in the same way for derivatives of these distributions.

### Installation from git

If your distribution does not already have a package for ./play.it, you can install it from this git repository:

```
git clone --branch main --depth 1 https://git.dotslashplay.it/scripts play.it.git
cd play.it.git
make
make install
```

## Game scripts

Starting with ./play.it 2.16 release, game scripts are no longer provided in this repository. You need to install a collection of game scripts in addition to the core library and wrapper to add support for some game installers. The following games collections are available:

- [Adventure games collection](https://git.dotslashplay.it/games-adventure/about/)
- [Puzzle games collection](https://git.dotslashplay.it/games-puzzle/about/)
- [Strategy games collection](https://git.dotslashplay.it/games-strategy/about/)
- [The community-maintained main collection](https://git.dotslashplay.it/games-community/about/)
- [vv221ʼs games collection](https://git.vv221.fr/play.it-vv221/about/)
- [Hoëlʼs games](https://git.dotslashplay.it/hoel/games-hoel/)
- [ahubʼs My Game Collection](https://git.dotslashplay.it/berru/games-berru/about/)
- [Caliban’s games collection](https://git.dotslashplay.it/caliban/games-caliban/)
- [ArRay’s game collection](https://git.dotslashplay.it/array/game-collection/)

## Usage

Once ./play.it is installed, you can call it providing a supported game installer as the first argument to generate the packages. An example could be:

```
play.it ~/Downloads/setup_sid_meiers_alpha_centauri_2.0.2.23.exe
```

The building process can take from a couple seconds to several minutes, depending mostly on the game size, and ends with the command to run as root to install the generated packages. On Debian, this could be something like:

```
apt install \
    /home/user/Downloads/alpha-centauri_6.0b-gog2.0.2.23+20221005.2_i386.deb \
    /home/user/Downloads/alpha-centauri-movies_6.0b-gog2.0.2.23+20221005.2_all.deb \
    /home/user/Downloads/alpha-centauri-data_6.0b-gog2.0.2.23+20221005.2_all.deb
```

## Contributing

### First contribution

There is no real rule for your first contribution. You can host your updated code anywhere you like and contact us via any way described in the [Contact information] section below. It is OK to expose your updated code through GitHub or GitLab.com if you have nothing better at hand, but we would be thankful if you would instead use some hosting outside of these big silos.

[Contact information]: #contact-information

We do not enforce any workflow like merge/pull requests or any such thing. We are interested in the result of your work, not in how you got to it. So shatter your shackles, and for once take pleasure in working *however you like*! You are not even required to use git if you do not know or do not like this tool.

Please try to follow these simple guidelines, and your contribution is probably going to be accepted quickly:

- Run `make check` before submitting your code, to ensure you did not break anything by accident;
- Use tabs for indentation. No real developer would ever indent anything with spaces.

### Regular contributions

If you keep contributing on a more regular basis (sending 3 patches in the same year would already make you a regular) we can grant you direct write access to the repositories hosted at git.dotslashplay.it. This is not mandatory, if for some reason you can not or do not want to work with git you can simply keep following the "First contribution" guidelines, and forget about this whole "Regular contributions" section.

To grant you such an access we need nothing but a public SSH key, that you can send us through any way described in the [Contact information] section below.

[Contact information]: #contact-information

Once you have been granted access, you should add the following to your local SSH configuration:

```
Host git.dotslashplay.it
    Port 1962
    User gitolite3
```

You should then update the remote of your local repository, with the following command (assuming a remote named "upstream"):

```
git remote set-url --push upstream ssh://git.dotslashplay.it/scripts
```

Since these repositories are worked on by several people, there are a couple extra guidelines that you should follow:

- Your work should always be pushed to a dedicated branch, never on the main branch;
- Bugfixes and messages improvements (including adding new error cases and error messages) should be pushed to branches named with a "fix/" prefix;
- New features and major rewrites should be pushed to branches named with a "feature/" prefix;
- You are allowed to push code to branches opened by other contributors, but please communicate with them if you plan to do so;
- Force push and branches deletion are not allowed, if you want a branch to be deleted please ask us to do it for you.

## Contact information

### IRC channel

Some ./play.it developers and users can be reached on IRC, channel is [#play.it] on network irc.oftc.net. The main language on this IRC channel is English, but most of us can speak French too.

[#play.it]: https://webchat.oftc.net/?channels=#play.it

### E-mail

A contact e-mail for feedback can usually be found in each ./play.it game script, as well as in the library. Open one of these files with any text editor to see the contact e-mail.

### Fediverse

./play.it has an account on the Fediverse, you can follow it for news about the development or use it to contact us: [@playit@fediverse.dotslashplay.it]

[@playit@fediverse.dotslashplay.it]: https://fediverse.dotslashplay.it/snac/playit
