# Run checks on the path used for temporary files
# USAGE: temporary_directory_checks $temporary_directory_path
temporary_directory_checks() {
	local temporary_directory_path
	temporary_directory_path="$1"

	# Skip all checks if no file operation is going to take place
	local noop_option noop_option_value
	for noop_option in \
		'help' \
		'list-available-scripts' \
		'list-packages' \
		'list-requirements' \
		'list-supported-games' \
		'show-game-script' \
		'version'
	do
		noop_option_value=$(option_value "$noop_option")
		## Setting a default value to 0 allows calling this function even when some of these options are not set,
		## making it easier to use in unit tests.
		if [ "${noop_option_value:-0}" -eq 1 ]; then
			return 0
		fi
	done

	# Check that the given path is an existing directory
	if [ ! -d "$temporary_directory_path" ]; then
		error_temporary_path_not_a_directory "$temporary_directory_path"
		return 1
	fi

	# Check that the given path is writable
	if [ ! -w "$temporary_directory_path" ]; then
		error_temporary_path_not_writable "$temporary_directory_path"
		return 1
	fi

	# Check that the given path is on a case-sensitive filesystem
	if ! check_directory_is_case_sensitive "$temporary_directory_path"; then
		error_temporary_path_not_case_sensitive "$temporary_directory_path"
		return 1
	fi

	# Check that the given path is on a filesystem with support for UNIX permissions
	if ! check_directory_supports_unix_permissions "$temporary_directory_path"; then
		error_temporary_path_no_unix_permissions "$temporary_directory_path"
		return 1
	fi

	# Check that the given path has allow use of the execution bit
	if ! check_directory_supports_executable_files "$temporary_directory_path"; then
		error_temporary_path_noexec "$temporary_directory_path"
		return 1
	fi

	# Check that there is enough free space under the given path
	local option_free_space_check
	option_free_space_check=$(option_value 'free-space-check')
	if [ "$option_free_space_check" -eq 1 ]; then
		temporary_directory_check_free_space "$temporary_directory_path"
	fi
}

# Check that there is enough free space under the given path
# USAGE: temporary_directory_check_free_space $path
# RETURN: 0 if there is enough space,
#         1 otherwise
temporary_directory_check_free_space() {
	local path
	path="$1"

	# Compute the total size of the archives contents
	local free_space_required archives_list archive archive_size
	free_space_required=0
	archives_list=$(archives_used_list)
	for archive in $archives_list; do
		archive_size=$(archive_size "$archive")
		free_space_required=$((free_space_required + archive_size))
	done

	# Compare the free space available with twice the archives contents size
	local df_options free_space_available free_space_required_double
	df_options='--block-size=1K --output=avail'
	free_space_available=$(env --ignore-environment df $df_options "$path" | tail --lines=1)
	free_space_required_double=$((free_space_required * 2))
	if [ "$free_space_available" -lt "$free_space_required_double" ]; then
		error_temporary_path_not_enough_space "$path"
		return 1
	fi
}

# Delete the temporary directory
# USAGE: working_directory_cleanup
working_directory_cleanup() {
	# Throw an error if the path to the temporary directory is not set yet
	if [ -z "${PLAYIT_WORKDIR:-}" ]; then
		error_working_directory_unset
		exit 1
	fi

	rm --force --recursive "${PLAYIT_WORKDIR:-}"
}

