# Print the suffix of the identifier of the current archive.
# USAGE: current_archive_suffix
# RETURN: the current archive identifier suffix including the leading underscore,
#         or an empty string if no archive is set
current_archive_suffix() {
	local archive
	archive=$(current_archive)

	printf '%s' "${archive#ARCHIVE_BASE}"
}

# Print the suffix of the identifier of the current package.
# USAGE: current_package_suffix
# RETURN: the current package identifier suffix including the leading underscorem
#         or an empty string of no current nor default package is set
current_package_suffix() {
	local package
	package=$(current_package)

	printf '%s' "${package#PKG}"
}

# Print the name of the variable containing the context-specific value of the given variable
# Context priority order is the following one:
# - archive-specific
# - package-specific
# - default
# - empty
# USAGE: context_name $variable_name
# RETURN: the name of the variable containing the context-specific value,
#         or an empty string
context_name() {
	local variable_name
	variable_name="$1"

	# Try to find an archive-specific value for the given variable.
	local context_name_archive
	context_name_archive=$(context_name_archive "$variable_name")
	if [ -n "$context_name_archive" ]; then
		printf '%s' "$context_name_archive"
		return 0
	fi

	# Try to find a package-specific value for the given variable.
	local context_name_package
	context_name_package=$(context_name_package "$variable_name")
	if [ -n "$context_name_package" ]; then
		printf '%s' "$context_name_package"
		return 0
	fi

	# Check if the base variable value is set.
	if ! variable_is_empty "$variable_name"; then
		printf '%s' "$variable_name"
		return 0
	fi

	# If no value has been found for the given variable, an empty string is returned.
}

# Print the name of the variable containing the archive-specific value of the given variable
# USAGE: context_name_archive $variable_name
# RETURN: the name of the variable containing the archive-specific value,
#         or an empty string
context_name_archive() {
	local variable_name
	variable_name="$1"

	local current_archive_suffix
	current_archive_suffix=$(current_archive_suffix)
	# Return early if no archive context is set
	if [ -z "$current_archive_suffix" ]; then
		return 0
	fi

	local current_archive_name
	while [ -n "$current_archive_suffix" ]; do
		current_archive_name="${variable_name}${current_archive_suffix}"
		if ! variable_is_empty "$current_archive_name"; then
			printf '%s' "$current_archive_name"
			return 0
		fi
		current_archive_suffix="${current_archive_suffix%_*}"
	done

	# If no value has been found for the given variable, an empty string is returned.
}

# Print the name of the variable containing the package-specific value of the given variable
# USAGE: context_name_package $variable_name
# RETURN: the name of the variable containing the package-specific value,
#         or an empty string
context_name_package() {
	local variable_name
	variable_name="$1"

	local current_package_suffix
	current_package_suffix=$(current_package_suffix)
	# Return early if no package context is set
	if [ -z "$current_package_suffix" ]; then
		return 0
	fi

	local current_package_name
	while [ -n "$current_package_suffix" ]; do
		current_package_name="${variable_name}${current_package_suffix}"
		if ! variable_is_empty "$current_package_name"; then
			printf '%s' "$current_package_name"
			return 0
		fi
		current_package_suffix="${current_package_suffix%_*}"
	done

	# If no value has been found for the given variable, an empty string is returned.
}

