# List the games supported by the current script
# USAGE: games_list_supported
# RETURN: a list of games,
#         separated by line breaks,
#         using the following format for each line:
#         game-id | Game name
games_list_supported() {
	local archives_list
	archives_list=$(archives_list)

	local archive game_id game_name
	for archive in $archives_list; do
		# Using a subshell to prevent the context archive setting from leaking outside of this function is not required
		set_current_archive "$archive"
		game_id=$(game_id)
		game_name=$(game_name)
		printf '%s | %s\n' "$game_id" "$game_name"
	done | sort --unique
}

# List all games supported by the available scripts
# USAGE: games_list_supported_all
# RETURN: a list of games,
#         separated by line breaks,
#         using the following format for each line:
#         game-id | Game name
games_list_supported_all() {
	local scripts_list
	scripts_list=$(games_list_scripts_all)

	local available_threads
	available_threads=$(nproc)
	## Passing the --list-supported-games switch is not required,
	## because $PLAYIT_OPTION_LIST_SUPPORTED_GAMES is already set.
	unset PLAYIT_COMPATIBILITY_LEVEL
	unset target_version
	printf '%s' "$scripts_list" |
		xargs --delimiter='\n' --max-args=1 --max-procs="$available_threads" sh |
		sort --unique
}

