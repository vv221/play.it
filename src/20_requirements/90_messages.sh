# Error - A required dependency is missing
# USAGE: error_dependency_not_found $command_name
error_dependency_not_found() {
	local command_name provider_package
	command_name="$1"
	provider_package=$(dependency_provided_by "$command_name")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='%s est introuvable. Installez %s avant de lancer ce script.\n'
		;;
		('en'|*)
			message='%s not found. Install %s before running this script.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$command_name" \
		"$provider_package"
}

# Error - A required dependency is available, but in a build that is too old
# USAGE: error_requirement_too_old $command_name $version_available $version_required
error_requirement_too_old() {
	local command_name version_available version_required
	command_name="$1"
	version_available="$2"
	version_required="$3"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='%s est disponible dans la version %s, mais la version %s ou plus récente est requise.\n'
		;;
		('en'|*)
			message='%s is available in version %s, but version %s or newer is required.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$command_name" \
		"$version_available" \
		"$version_required"
}

