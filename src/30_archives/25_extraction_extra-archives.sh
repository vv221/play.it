# Extract the contents from the extra archives providing required libraries
# USAGE: archive_extraction_extra_libraries
archive_extraction_extra_libraries() {
	local libraries_required library_required
	libraries_required=$(dependencies_list_native_libraries_all)
	while read -r library_required; do
		case "$library_required" in
			('libcurl.so.4+CURL_OPENSSL_3')
				archive_extraction_extra_libcurl3
			;;
			('libFLAC.so.8')
				archive_extraction_extra_libflac8
			;;
			('libgconf-2.so.4')
				archive_extraction_extra_libgconf2
			;;
			('libidn.so.11')
				archive_extraction_extra_libidn11
			;;
			('libpng12.so.0')
				archive_extraction_extra_libpng12
			;;
			('libssl.so.1.0.0')
				archive_extraction_extra_libssl100
			;;
			('libssl.so.1.1')
				archive_extraction_extra_libssl11
			;;
		esac
	done <<- EOL
	$(printf '%s' "$libraries_required")
	EOL
}

# Extract libcurl.so.3 and libcurl.so.4 including the CURL_OPENSSL_3 symbol
# USAGE: archive_extraction_extra_libcurl3
archive_extraction_extra_libcurl3() {
	archive_extraction 'ARCHIVE_LIBCURL3'
}

# Extract libFLAC.so.8
# USAGE: archive_extraction_extra_libflac8
archive_extraction_extra_libflac8() {
	# On Arch Linux, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			return 0
		;;
	esac

	archive_extraction 'ARCHIVE_LIBFLAC8'
}

# Extract GConf 2 library
# USAGE: archive_extraction_extra_libgconf2
archive_extraction_extra_libgconf2() {
	archive_extraction 'ARCHIVE_LIBGCONF2'
}

# Extract GNU Libidn 11 library
# USAGE: archive_extraction_extra_libidn11
archive_extraction_extra_libidn11() {
	# On Arch Linux, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			return 0
		;;
	esac

	archive_extraction 'ARCHIVE_LIBIDN11'
}

# Extract PNG 1.2 libraries
# USAGE: archive_extraction_extra_libpng12
archive_extraction_extra_libpng12() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	archive_extraction 'ARCHIVE_LIBPNG12'
}

# Extract OpenSSL 1.0.0 libraries
# USAGE: archive_extraction_extra_libssl100
archive_extraction_extra_libssl100() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	archive_extraction 'ARCHIVE_OPENSSL100'
}

# Extract OpenSSL 1.1 libraries
# USAGE: archive_extraction_extra_libssl11
archive_extraction_extra_libssl11() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	archive_extraction 'ARCHIVE_OPENSSL11'
}

# Extract the content of an icons pack archive
# USAGE: archive_extraction_extra_icons
archive_extraction_extra_icons() {
	archive_extraction 'ARCHIVE_ICONS'
}

