# Check for the availability of commands required to extract the content from AppImage archives
# USAGE: archive_dependencies_check_type_appimage
archive_dependencies_check_type_appimage() {
	local required_command
	for required_command in 'binwalk' 'unsquashfs'; do
		if ! command -v "$required_command" >/dev/null 2>&1; then
			error_dependency_not_found "$required_command"
			return 1
		fi
	done
}

# Extract the content of an AppImage archive
# USAGE: archive_extraction_appimage $archive $destination_directory $log_file
archive_extraction_appimage() {
	local archive destination_directory log_file
	archive="$1"
	destination_directory="$2"
	log_file="$3"

	local archive_path
	archive_path=$(archive_path "$archive")

	local archive_offset
	archive_offset=$(binwalk "$archive_path" | sed --silent 's/\(^[0-9]\+\).*Squashfs filesystem.*/\1/p')

	local archive_extraction_return_code
	{
		printf 'unsquashfs -offset "%s" -dest "%s" "%s"\n' "$archive_offset" "$destination_directory" "$archive_path"
		unsquashfs -offset "$archive_offset" -dest "$destination_directory" "$archive_path"
		archive_extraction_return_code=$?
	} >> "$log_file" || true
	if [ $archive_extraction_return_code -ne 0 ]; then
		error_archive_extraction_failure "$archive"
		return 1
	fi
}

