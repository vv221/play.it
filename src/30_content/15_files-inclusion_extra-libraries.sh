# Include required native libraries provided by extra archives
# USAGE: content_inclusion_extra_libraries $package
content_inclusion_extra_libraries() {
	local package
	package="$1"

	local libraries_required library_required
	libraries_required=$(dependencies_list_native_libraries "$package")
	while read -r library_required; do
		case "$library_required" in
			('libcurl.so.4+CURL_OPENSSL_3')
				content_inclusion_extra_libraries_libcurl3 "$package"
			;;
			('libFLAC.so.8')
				content_inclusion_extra_libraries_libflac8 "$package"
			;;
			('libgconf-2.so.4')
				content_inclusion_extra_libraries_libgconf2 "$package"
			;;
			('libidn.so.11')
				content_inclusion_extra_libraries_libidn11 "$package"
			;;
			('libpng12.so.0')
				content_inclusion_extra_libraries_libpng12 "$package"
			;;
			('libssl.so.1.0.0')
				content_inclusion_extra_libraries_libssl100 "$package"
			;;
			('libssl.so.1.1')
				content_inclusion_extra_libraries_libssl11 "$package"
			;;
		esac
	done <<- EOL
	$(printf '%s' "$libraries_required")
	EOL
}

# Include libcurl.so.3 and libcurl.so.4 including the CURL_OPENSSL_3 symbol
# USAGE: content_inclusion_extra_libraries_libcurl3 $package
content_inclusion_extra_libraries_libcurl3() {
	local package
	package="$1"

	# Set the list of files to include from the provided archive
	local package_architecture CONTENT_LIBCURL3_PATH CONTENT_LIBCURL3_FILES
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			## Silence ShellCheck false-positive
			## CONTENT_LIBCURL3_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBCURL3_PATH='x86_32'
		;;
		('64')
			## Silence ShellCheck false-positive
			## CONTENT_LIBCURL3_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBCURL3_PATH='x86_64'
		;;
	esac
	## Silence ShellCheck false-positive
	## CONTENT_LIBCURL3_FILES appears unused. Verify use (or export if used externally).
	# shellcheck disable=SC2034
	CONTENT_LIBCURL3_FILES='
	libcrypto.so.1.0.2
	libssl.so.1.0.2
	libcurl.so.3
	libcurl.so.4
	libcurl.so.4.4.0'

	# Proceed with the actual files inclusion
	local path_libraries
	path_libraries=$(
		set_current_package "$package"
		path_libraries
	)
	content_inclusion 'LIBCURL3' "$package" "$path_libraries"

	# Update the list of dependencies on native libraries
	local extra_native_libraries_required
	extra_native_libraries_required='
	libcom_err.so.2
	libc.so.6
	libdl.so.2
	libgssapi_krb5.so.2
	libidn2.so.0
	libk5crypto.so.3
	libkrb5.so.3
	libnghttp2.so.14
	libpsl.so.5
	libpthread.so.0
	librtmp.so.1
	libssh2.so.1
	libz.so.1'
	dependencies_add_native_libraries "$package" "$extra_native_libraries_required"
}

# Include libFLAC.so.8
# USAGE: content_inclusion_extra_libraries_libflac8 $package
content_inclusion_extra_libraries_libflac8() {
	# On Arch Linux, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			return 0
		;;
	esac

	local package
	package="$1"

	# Set the list of files to include from the provided archive
	local package_architecture CONTENT_LIBFLAC8_PATH CONTENT_LIBFLAC8_FILES
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			## Silence ShellCheck false-positive
			## CONTENT_LIBFLAC8_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBFLAC8_PATH='i386-linux-gnu'
		;;
		('64')
			## Silence ShellCheck false-positive
			## CONTENT_LIBFLAC8_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBFLAC8_PATH='x86_64-linux-gnu'
		;;
	esac
	## Silence ShellCheck false-positive
	## CONTENT_LIBFLAC8_FILES appears unused. Verify use (or export if used externally).
	# shellcheck disable=SC2034
	CONTENT_LIBFLAC8_FILES='
	libFLAC.so.8
	libFLAC.so.8.3.0'

	# Proceed with the actual files inclusion
	local path_libraries
	path_libraries=$(
		set_current_package "$package"
		path_libraries
	)
	content_inclusion 'LIBFLAC8' "$package" "$path_libraries"

	# Update the list of dependencies on native libraries
	local extra_native_libraries_required
	extra_native_libraries_required='
	libc.so.6
	libm.so.6
	libogg.so.0'
	dependencies_add_native_libraries "$package" "$extra_native_libraries_required"
}

# Include GConf 2 library
# USAGE: content_inclusion_extra_libraries_libgconf2 $package
content_inclusion_extra_libraries_libgconf2() {
	local package
	package="$1"

	# Set the list of files to include from the provided archive
	local package_architecture CONTENT_LIBGCONF2_PATH CONTENT_LIBGCONF2_FILES
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			## Silence ShellCheck false-positive
			## CONTENT_LIBGCONF2_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBGCONF2_PATH='i386-linux-gnu'
		;;
		('64')
			## Silence ShellCheck false-positive
			## CONTENT_LIBGCONF2_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBGCONF2_PATH='x86_64-linux-gnu'
		;;
	esac
	## Silence ShellCheck false-positive
	## CONTENT_LIBGCONF2_FILES appears unused. Verify use (or export if used externally).
	# shellcheck disable=SC2034
	CONTENT_LIBGCONF2_FILES='
	libgconf-2.so.4
	libgconf-2.so.4.1.5'

	# Proceed with the actual files inclusion
	local path_libraries
	path_libraries=$(
		set_current_package "$package"
		path_libraries
	)
	content_inclusion 'LIBGCONF2' "$package" "$path_libraries"

	# Update the list of dependencies on native libraries
	local extra_native_libraries_required
	extra_native_libraries_required='
	libc.so.6
	libdbus-1.so.3
	libdbus-glib-1.so.2
	libglib-2.0.so.0
	libgmodule-2.0.so.0
	libgobject-2.0.so.0'
	dependencies_add_native_libraries "$package" "$extra_native_libraries_required"
}

# Include GNU Libidn library
# USAGE: content_inclusion_extra_libraries_libidn11 $package
content_inclusion_extra_libraries_libidn11() {
	# On Arch Linux, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			return 0
		;;
	esac

	local package
	package="$1"

	# Set the list of files to include from the provided archive
	local package_architecture CONTENT_LIBIDN11_PATH CONTENT_LIBIDN11_FILES
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			## Silence ShellCheck false-positive
			## CONTENT_LIBIDN11_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBIDN11_PATH='i386-linux-gnu'
		;;
		('64')
			## Silence ShellCheck false-positive
			## CONTENT_LIBIDN11_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBIDN11_PATH='x86_64-linux-gnu'
		;;
	esac
	## Silence ShellCheck false-positive
	## CONTENT_LIBIDN11_FILES appears unused. Verify use (or export if used externally).
	# shellcheck disable=SC2034
	CONTENT_LIBIDN11_FILES='
	libidn.so.11
	libidn.so.11.6.16'

	# Proceed with the actual files inclusion
	local path_libraries
	path_libraries=$(
		set_current_package "$package"
		path_libraries
	)
	content_inclusion 'LIBIDN11' "$package" "$path_libraries"

	# Update the list of dependencies on native libraries
	local extra_native_libraries_required
	extra_native_libraries_required='
	libc.so.6'
	dependencies_add_native_libraries "$package" "$extra_native_libraries_required"
}

# Include PNG 1.2 libraries
# USAGE: content_inclusion_extra_libraries_libpng12 $package
content_inclusion_extra_libraries_libpng12() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	local package
	package="$1"

	# Set the list of files to include from the provided archive
	local package_architecture CONTENT_LIBPNG12_PATH CONTENT_LIBPNG12_FILES
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			## Silence ShellCheck false-positive
			## CONTENT_LIBPNG12_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBPNG12_PATH='x86_32'
		;;
		('64')
			## Silence ShellCheck false-positive
			## CONTENT_LIBPNG12_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_LIBPNG12_PATH='x86_64'
		;;
	esac
	## Silence ShellCheck false-positive
	## CONTENT_LIBPNG12_FILES appears unused. Verify use (or export if used externally).
	# shellcheck disable=SC2034
	CONTENT_LIBPNG12_FILES='
	libpng12.so.0
	libpng12.so.0.50.0'

	# Proceed with the actual files inclusion
	local path_libraries
	path_libraries=$(
		set_current_package "$package"
		path_libraries
	)
	content_inclusion 'LIBPNG12' "$package" "$path_libraries"

	# Update the list of dependencies on native libraries
	local extra_native_libraries_required
	extra_native_libraries_required='
	libc.so.6
	libm.so.6
	libz.so.1'
	dependencies_add_native_libraries "$package" "$extra_native_libraries_required"
}

# Include OpenSSL 1.0.0 libraries
# USAGE: content_inclusion_extra_libraries_libssl100 $package
content_inclusion_extra_libraries_libssl100() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	local package
	package="$1"

	# Set the list of files to include from the provided archive
	local package_architecture CONTENT_OPENSSL100_PATH CONTENT_OPENSSL100_FILES
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			## Silence ShellCheck false-positive
			## CONTENT_OPENSSL100_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_OPENSSL100_PATH='x86_32'
		;;
		('64')
			## Silence ShellCheck false-positive
			## CONTENT_OPENSSL100_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_OPENSSL100_PATH='x86_64'
		;;
	esac
	## Silence ShellCheck false-positive
	## CONTENT_OPENSSL100_FILES appears unused. Verify use (or export if used externally).
	# shellcheck disable=SC2034
	CONTENT_OPENSSL100_FILES='
	libcrypto.so.1.0.0
	libssl.so.1.0.0'

	# Proceed with the actual files inclusion
	local path_libraries
	path_libraries=$(
		set_current_package "$package"
		path_libraries
	)
	content_inclusion 'OPENSSL100' "$package" "$path_libraries"

	# Update the list of dependencies on native libraries
	local extra_native_libraries_required
	extra_native_libraries_required='
	libc.so.6
	libdl.so.2'
	dependencies_add_native_libraries "$package" "$extra_native_libraries_required"
}

# Include OpenSSL 1.1 libraries
# USAGE: content_inclusion_extra_libraries_libssl11 $package
content_inclusion_extra_libraries_libssl11() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	local package
	package="$1"

	# Set the list of files to include from the provided archive
	local package_architecture CONTENT_OPENSSL11_PATH CONTENT_OPENSSL11_FILES
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			## Silence ShellCheck false-positive
			## CONTENT_OPENSSL11_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_OPENSSL11_PATH='i386-linux-gnu'
		;;
		('64')
			## Silence ShellCheck false-positive
			## CONTENT_OPENSSL11_PATH appears unused. Verify use (or export if used externally).
			# shellcheck disable=SC2034
			CONTENT_OPENSSL11_PATH='x86_64-linux-gnu'
		;;
	esac
	## Silence ShellCheck false-positive
	## CONTENT_OPENSSL11_FILES appears unused. Verify use (or export if used externally).
	# shellcheck disable=SC2034
	CONTENT_OPENSSL11_FILES='
	libcrypto.so.1.1
	libssl.so.1.1'

	# Proceed with the actual files inclusion
	local path_libraries
	path_libraries=$(
		set_current_package "$package"
		path_libraries
	)
	content_inclusion 'OPENSSL11' "$package" "$path_libraries"

	# Update the list of dependencies on native libraries
	local extra_native_libraries_required
	extra_native_libraries_required='
	libc.so.6
	libdl.so.2
	libpthread.so.0'
	dependencies_add_native_libraries "$package" "$extra_native_libraries_required"
}

