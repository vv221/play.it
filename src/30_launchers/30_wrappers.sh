# Print a wrapper function spawning a graphical terminal
# USAGE: launcher_wrapper_terminal
launcher_wrapper_terminal() {
	cat <<- 'EOF'
	# Spawn a terminal with support for the -e option to run arbitrary commands
	terminal_wrapper() {
	    for command in \
	        x-terminal-emulator \
	        xterm
	    do
	        if command -v $command >/dev/null; then
	            printf '%s' "$command"
	            return 0
	        fi
	    done
	}

	EOF
}

