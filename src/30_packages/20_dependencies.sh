# Print the list of generic dependencies required by a given package
# USAGE: dependencies_list_generic $package
# RETURN: a list of generic dependcy keywords,
#         separated by line breaks
dependencies_list_generic() {
	local package
	package="$1"

	local dependencies_generic
	dependencies_generic=$(context_value "${package}_DEPS")

	# Generic dependencies are deprecated for compatibility levels ≥ 2.30
	if
		[ -n "$dependencies_generic" ] &&
		compatibility_level_is_at_least '2.30'
	then
		warning_deprecated_variable "${package}_DEPS" "${package}_DEPENDENCIES_xxx"
	fi

	# Return early if the current package does not use legacy generic dependencies
	if [ -z "$dependencies_generic" ]; then
		return 0
	fi

	printf '%s' "$dependencies_generic" | sed 's/ /\n/g' | list_clean
}

