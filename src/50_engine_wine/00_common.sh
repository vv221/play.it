# WINE - Print the paths relative to the WINE prefix that should be diverted to persistent storage
# USAGE: wine_persistent_directories
# RETURN: A list of path to directories,
#         separated by line breaks.
wine_persistent_directories() {
	local persistent_directories
	persistent_directories=$(context_value 'WINE_PERSISTENT_DIRECTORIES')

	# Fall back on the default list of directories for the current game engine
	if [ -z "$persistent_directories" ]; then
		local game_engine
		game_engine=$(game_engine)
		case "$game_engine" in
			('unrealengine4')
				persistent_directories=$(unrealengine4_wine_persistent_directories_default)
			;;
		esac
	fi

	printf '%s' "$persistent_directories"
}

