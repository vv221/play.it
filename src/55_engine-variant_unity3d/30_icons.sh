# Unity3D - Compute the icon path from the UNITY3D_NAME value
# USAGE: unity3d_icon_path
# RETURN: the path to the icon file
unity3d_icon_path() {
	local icon_path unity3d_name application application_type
	unity3d_name=$(unity3d_name)
	application=$(icon_application "$icon")
	application_type=$(application_type "$application")

	## Throw an error if no application type is found,
	## this is unexpected when relying on the default icon path for Unity3D games.
	if [ -z "$application_type" ]; then
		error_no_application_type "$application"
		return 1
	fi

	case "$application_type" in
		('native')
			icon_path="${unity3d_name}_Data/Resources/UnityPlayer.png"
		;;
		('wine')
			icon_path="${unity3d_name}.exe"
		;;
	esac

	printf '%s' "${icon_path:-}"
}

