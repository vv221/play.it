# Gentoo - Print the content of the "KEYWORDS" field
# USAGE: gentoo_field_keywords $package
# RETURN: the field value
gentoo_field_keywords() {
	local package
	package="$1"

	local package_architecture ebuild_keywords
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			ebuild_keywords='-* x86 amd64'
		;;
		('64')
			ebuild_keywords='-* amd64'
		;;
		(*)
			ebuild_keywords='x86 amd64' # data packages
		;;
	esac

	printf '%s' "$ebuild_keywords"
}

# Gentoo - Print the content of the "DESCRIPTION" field
# USAGE: gentoo_field_description $package
# RETURN: the field value
gentoo_field_description() {
	local package
	package="$1"

	local game_name package_description script_version_string
	game_name=$(game_name)
	package_description=$(package_description "$package")
	script_version_string=$(script_version)

	printf '%s' "$game_name"
	if [ -n "$package_description" ]; then
		printf -- ' - %s' "$package_description"
	fi
	printf -- ' - ./play.it script version %s' "$script_version_string"
}

# Gentoo - Print the content of the "RDEPEND" field
# USAGE: gentoo_field_rdepend $package
# RETURN: the field value
gentoo_field_rdepend() {
	local package
	package="$1"

	local dependencies_list first_item_displayed dependency_string
	dependencies_list=$(dependencies_gentoo_full_list "$package")
	first_item_displayed=0
	while IFS= read -r dependency_string; do
		if [ -z "$dependency_string" ]; then
			continue
		fi
		# Gentoo policy is that dependencies should be displayed one per line,
		# and indentation is to be done using tabulations.
		if [ "$first_item_displayed" -eq 0 ]; then
			printf '%s' "$dependency_string"
			first_item_displayed=1
		else
			printf '\n\t%s' "$dependency_string"
		fi
	done <<- EOL
	$(printf '%s' "$dependencies_list")
	EOL

	# Return early when building package in the "egentoo" format,
	# as we have no good way to set conflicts with this format.
	local option_package
	option_package=$(option_value 'package')
	if [ "$option_package" = 'egentoo' ]; then
		return 0
	fi

	local package_conflicts package_conflict
	package_conflicts=$(package_provides "$package")

	# Return early if the current package has no "provides" field.
	if [ -z "$package_conflicts" ]; then
		return 0
	fi

	# Gentoo has no notion of "provided" package,
	# so we need to loop over all supported archives
	# to get the name of all packages providing a given package id.
	local archives_list packages_list
	archives_list=$(archives_list)
	packages_list=$(
		for archive in $archives_list; do
			set_current_archive "$archive"
			packages_list
		done | list_clean
	)

	# For each conflict of the current package,
	# find all potential packages that would provide this package id.
	local package_current package_current_id package_current_provides package_current_provide
	for package_conflict in $package_conflicts; do
		for package_current in $packages_list; do
			# Skip the package we are writing metadata for,
			# so it does not end up conflicting with itself.
			if [ "$package_current" = "$package" ]; then
				continue
			fi
			package_current_provides=$(package_provides "$package_current")
			for package_current_provide in $package_current_provides; do
				if [ "$package_current_provide" = "$package_conflict" ]; then
					package_current_id=$(package_id "$package_current")
					if [ "$first_item_displayed" -eq 0 ]; then
						printf '!games-play.it/%s' "$package_current_id"
						first_item_displayed=1
					else
						printf '\n\t!games-play.it/%s' "$package_current_id"
					fi
				fi
			done
		done
	done
}

