# Gentoo - Print the pkg_postinst function
# USAGE: gentoo_script_postinst $package
# RETURN: the defintion of the pkg_postinst function,
#         spanning over several lines
gentoo_script_postinst() {
	local package
	package="$1"

	local postinst_actions postinst_warnings
	postinst_actions=$(package_postinst_actions "$package")
	postinst_warnings=$(package_postinst_warnings "$package")
	if [ -n "$postinst_actions" ] || [ -n "$postinst_warnings" ]; then
		cat <<- EOF
		pkg_postinst() {
		EOF

		# Include actions that should be run.
		if [ -n "$postinst_actions" ]; then
			printf '%s\n' "$postinst_actions"
		fi

		# Include warnings that should be displayed.
		if [ -n "$postinst_warnings" ]; then
			local warning_line
			while read -r warning_line; do
				printf 'ewarn "%s"\n' "$warning_line"
			done <<- EOL
			$(printf '%s' "$postinst_warnings")
			EOL
		fi

		cat <<- EOF
		}
		EOF
	fi
}

# Gentoo - Print the pkg_prerm function
# USAGE: gentoo_script_prerm $package
# RETURN: the defintion of the pkg_prerm function,
#         spanning over several lines
gentoo_script_prerm() {
	local package
	package="$1"

	local prerm_actions
	prerm_actions=$(package_prerm_actions "$package")
	if [ -n "$prerm_actions" ]; then
		cat <<- EOF
		pkg_prerm() {
		$prerm_actions
		}
		EOF
	fi
}

