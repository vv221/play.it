#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

setUp() {
	# Set a temporary directory to mess with real files
	TEST_TEMP_DIR=$(mktemp --directory)
	export TEST_TEMP_DIR
}

tearDown() {
	rm --force --recursive "$TEST_TEMP_DIR"
}

test_archives_path_base() {
	local PLAYIT_ARCHIVES_PATH_BASE

	PLAYIT_ARCHIVES_PATH_BASE='/home/jeux/alpha-centauri/archives/gog.com'
	archives_path_base=$(archives_path_base)
	assertEquals '/home/jeux/alpha-centauri/archives/gog.com' "$archives_path_base"
	unset PLAYIT_ARCHIVES_PATH_BASE

	# Falls back on $PWD if $PLAYIT_ARCHIVES_PATH_BASE is not set
	archives_path_base=$(archives_path_base)
	assertEquals "$PWD" "$archives_path_base"
}

test_archives_integrity_check_md5() {
	local PLAYIT_ARCHIVES_USED_LIST PLAYIT_WORKDIR ARCHIVE_BASE_0_MD5

	PLAYIT_ARCHIVES_USED_LIST='ARCHIVE_BASE_0'
	PLAYIT_WORKDIR="$TEST_TEMP_DIR"

	mkdir --parents "${PLAYIT_WORKDIR}/cache"
	cat >> "${PLAYIT_WORKDIR}/cache/hashes" <<- EOF
	ARCHIVE_BASE_0 | e70187d92fa120771db99dfa81679cfc
	EOF
	ARCHIVE_BASE_0_MD5='e70187d92fa120771db99dfa81679cfc'
	assertTrue 'archives_integrity_check_md5'
	rm "${PLAYIT_WORKDIR}/cache/hashes"

	mkdir --parents "${PLAYIT_WORKDIR}/cache"
	cat >> "${PLAYIT_WORKDIR}/cache/hashes" <<- EOF
	ARCHIVE_BASE_0 | 62298f00f1f2268c8d5004f5b2e9fc93
	EOF
	ARCHIVE_BASE_0_MD5='2ee16fab54493e1c2a69122fd2e56635'
	assertFalse 'archives_integrity_check_md5'

	mkdir --parents "${PLAYIT_WORKDIR}/cache"
	cat >> "${PLAYIT_WORKDIR}/cache/hashes" <<- EOF
	ARCHIVE_BASE_0 | eda90c314fd4d845ec9f3805b44de9f8
	EOF
	unset ARCHIVE_BASE_0_MD5
	assertTrue 'A hashsum mismatch has been triggered for an archive that has no expected MD5 hash.' 'archives_integrity_check_md5'
}

test_archives_used_list() {
	local archives_list archives_list_expected PLAYIT_ARCHIVES_USED_LIST

	PLAYIT_ARCHIVES_USED_LIST='ARCHIVE_BASE_0
	ARCHIVE_REQUIRED_ENGINE
	ARCHIVE_OPTIONAL_ICONS'
	archives_list=$(archives_used_list)
	archives_list_expected='ARCHIVE_BASE_0
ARCHIVE_REQUIRED_ENGINE
ARCHIVE_OPTIONAL_ICONS'
	assertEquals "$archives_list_expected" "$archives_list"
	unset PLAYIT_ARCHIVES_USED_LIST

	# The list can be empty
	archives_list=$(archives_used_list)
	assertNull "$archives_list"
}

test_archives_used_add() {
	local archives_list archives_list_expected PLAYIT_ARCHIVES_USED_LIST

	archives_used_add 'ARCHIVE_BASE_0'
	archives_list=$(archives_used_list)
	assertEquals 'ARCHIVE_BASE_0' "$archives_list"

	archives_used_add 'ARCHIVE_REQUIRED_ENGINE'
	archives_list=$(archives_used_list)
	archives_list_expected='ARCHIVE_BASE_0
ARCHIVE_REQUIRED_ENGINE'
	assertEquals "$archives_list_expected" "$archives_list"
}

test_archives_list() {
	local ARCHIVE_BASE_0 ARCHIVE_BASE_1 ARCHIVE_NOT_EXPECTED_FORMAT archives_list_expected
	ARCHIVE_BASE_0='some_game_archive.tar.gz'
	ARCHIVE_BASE_1='some_other_game_archive.tar.gz'
	ARCHIVE_BASE_OTHER_0='yet_another_game_archive.tar.gz'
	ARCHIVE_NOT_EXPECTED_FORMAT='some_game_archive_that_should_not_be_included.tar.gz'
	archives_list_expected='ARCHIVE_BASE_1
ARCHIVE_BASE_0
ARCHIVE_BASE_OTHER_0'
	assertEquals "$archives_list_expected" "$(archives_list)"
}
