#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_content_path_default() {
	local \
		content_path_default \
		CONTENT_PATH_DEFAULT \
		PLAYIT_CONTEXT_ARCHIVE CONTENT_PATH_DEFAULT_0

	CONTENT_PATH_DEFAULT='some/path'
	content_path_default=$(content_path_default)
	assertEquals \
		'content_path_default failed to get an explicitly set value from CONTENT_PATH_DEFAULT.' \
		'some/path' "$content_path_default"
	unset CONTENT_PATH_DEFAULT

	set_current_archive 'ARCHIVE_BASE_0'
	CONTENT_PATH_DEFAULT_0='some/other/path'
	content_path_default=$(content_path_default)
	assertEquals \
		'content_path_default failed to get a contextual value for CONTENT_PATH_DEFAULT.' \
		'some/other/path' "$content_path_default"
	unset ARCHIVE CONTENT_PATH_DEFAULT_0

	assertFalse \
		'content_path_default did not fail, despite no value being set for CONTENT_PATH_DEFAULT.' \
		'content_path_default'
}

test_content_path() {
	local \
		CONTENT_PATH_DEFAULT CONTENT_GAME_DATA_PATH CONTENT_GAME_DATA_PATH_0 \
		PLAYIT_CONTEXT_ARCHIVE

	CONTENT_PATH_DEFAULT='default/path'
	assertEquals "$CONTENT_PATH_DEFAULT" "$(content_path 'GAME_DATA')"

	CONTENT_GAME_DATA_PATH='specific/path'
	assertEquals "$CONTENT_GAME_DATA_PATH" "$(content_path 'GAME_DATA')"

	set_current_archive 'ARCHIVE_BASE_0'
	CONTENT_GAME_DATA_PATH_0='more/specific/path'
	assertEquals "$CONTENT_GAME_DATA_PATH_0" "$(content_path 'GAME_DATA')"
}

test_content_files() {
	local \
		CONTENT_GAME_DATA_FILES CONTENT_GAME_DATA_FILES_0 \
		PLAYIT_CONTEXT_ARCHIVE

	assertNull "$(content_files 'GAME_DATA')"

	CONTENT_GAME_DATA_FILES='some
list
of
files'
	assertEquals "$CONTENT_GAME_DATA_FILES" "$(content_files 'GAME_DATA')"

	set_current_archive 'ARCHIVE_BASE_0'
	CONTENT_GAME_DATA_FILES_0='specific
list
of
files'
	assertEquals "$CONTENT_GAME_DATA_FILES_0" "$(content_files 'GAME_DATA')"

	# Fallback lists of files for Unity3D and Unreal Engine 4 games are not tested here.
}
