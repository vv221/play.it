#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_huge_files_list() {
	local HUGE_FILES_MAIN huge_files

	HUGE_FILES_MAIN='
some-big-file.pak'
	huge_files=$(huge_files_list 'PKG_MAIN')
	assertEquals 'some-big-file.pak' "$huge_files"

	# Check that duplicates are dropped
	HUGE_FILES_MAIN='
some-duplicated-file.pak
some-duplicated-file.pak'
	huge_files=$(huge_files_list 'PKG_MAIN')
	assertEquals 'some-duplicated-file.pak' "$huge_files"
}

test_content_inclusion_chunk_single() {
	local \
		GAME_ID PACKAGES_LIST \
		PKG_DATA_ID PKG_DATA_DESCRIPTION \
		PKG_DATA_CHUNK1_ID PKG_DATA_CHUNK1_DESCRIPTION PKG_DATA_CHUNK1_PRERM_RUN \
		CONTENT_GAME_DATA_CHUNK1_FILES \
		packages_list packages_list_expected package_id package_description package_prerm_actions content_files

	# Skip the actual file inclusion
	path_game_data() {
		printf '/some/arbitrary/path'
	}
	content_inclusion() { return 0 ; }

	# Use a simplified function for returning the package description
	package_description() {
		context_value "${1}_DESCRIPTION"
	}

	PACKAGES_LIST='
	PKG_BIN
	PKG_DATA'
	PKG_DATA_ID='package-data'
	PKG_DATA_DESCRIPTION='data'
	content_inclusion_chunk_single 'PKG_DATA' 'huge-file.pak.1' '1' 'huge-file.pak'
	packages_list_expected='PKG_DATA_CHUNK1
PKG_BIN
PKG_DATA'
	packages_list=$(packages_list)
	assertEquals "$packages_list_expected" "$packages_list"
	package_id=$(package_id 'PKG_DATA_CHUNK1')
	assertEquals 'package-data-chunk1' "$package_id"
	package_description=$(package_description 'PKG_DATA_CHUNK1')
	assertEquals 'data - chunk 1' "$package_description"
	package_prerm_actions=$(package_prerm_actions 'PKG_DATA_CHUNK1')
	assertNotNull "$package_prerm_actions"
	content_files=$(content_files 'GAME_DATA_CHUNK1')
	assertEquals 'huge-file.pak.1' "$content_files"
}
