#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_icons_inclusion_single_application() {
	## Prevent actions requiring files manipulation.
	icons_inclusion_single_icon() { return 0; }

	# Ensure that an error is thrown when trying to extract icons from an application with no icon set
	assertFalse \
		'icons_inclusion_single_application failed to notice it has been asked to handle an application with no icon set.' \
		'icons_inclusion_single_application "PKG_MAIN" "APP_MAIN"'

	# Check that the previous error is not thrown if an optional icons archive is supported.
	local ARCHIVE_OPTIONAL_ICONS_NAME
	ARCHIVE_OPTIONAL_ICONS_NAME='alpha-centauri-icons.2022-10-04.tar.gz'
	assertTrue \
		'icons_inclusion_single_application failed to notice that an optional icons archive is supported.' \
		'icons_inclusion_single_application "PKG_MAIN" "APP_MAIN"'

	unset -f icons_inclusion_single_icon
}

test_icon_full_path() {
	local icon_full_path PLAYIT_WORKDIR CONTENT_PATH_DEFAULT APP_MAIN_ICON
	
	PLAYIT_WORKDIR='/var/tmp/play.it/jazz-jackrabbit-2.t3nm8'
	CONTENT_PATH_DEFAULT='app'
	APP_MAIN_ICON='jazz2.exe'
	icon_full_path=$(icon_full_path 'APP_MAIN_ICON')
	assertEquals '/var/tmp/play.it/jazz-jackrabbit-2.t3nm8/gamedata/app/jazz2.exe' "$icon_full_path"
}
