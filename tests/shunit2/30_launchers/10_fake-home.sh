#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_fake_home_persistent_directories() {
	local ARCHIVE FAKE_HOME_PERSISTENT_DIRECTORIES FAKE_HOME_PERSISTENT_DIRECTORIES_MAIN persistent_directories_expected persistent_directories
	local \
		persistent_directories persistent_directories_expected \
		FAKE_HOME_PERSISTENT_DIRECTORIES FAKE_HOME_PERSISTENT_DIRECTORIES_MAIN \
		PLAYIT_CONTEXT_ARCHIVE

	FAKE_HOME_PERSISTENT_DIRECTORIES='path/to/dir/1
path/to/dir/2'
	persistent_directories=$(fake_home_persistent_directories)
	persistent_directories_expected='path/to/dir/1
path/to/dir/2'
	assertEquals "$persistent_directories_expected" "$persistent_directories"
	unset FAKE_HOME_PERSISTENT_DIRECTORIES

	set_current_archive 'ARCHIVE_BASE_MAIN_0'
	FAKE_HOME_PERSISTENT_DIRECTORIES_MAIN='path/to/archive/specific/dir/1
path/to/archive/specific/dir/2'
	persistent_directories=$(fake_home_persistent_directories)
	persistent_directories_expected='path/to/archive/specific/dir/1
path/to/archive/specific/dir/2'
	assertEquals "$persistent_directories_expected" "$persistent_directories"
	unset ARCHIVE FAKE_HOME_PERSISTENT_DIRECTORIES_MAIN
}
