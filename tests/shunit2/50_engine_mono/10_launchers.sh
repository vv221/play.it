#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_mono_launcher() {
	local APP_MAIN_PREFIX_TYPE

	APP_MAIN_PREFIX_TYPE='symlinks'
	assertTrue \
		'Failed to generate a launcher for a Mono game using a symlinks prefix.' \
		'mono_launcher "APP_MAIN"'

	APP_MAIN_PREFIX_TYPE='none'
	assertTrue \
		'Failed to generate a launcher for a Mono game not using a prefix.' \
		'mono_launcher "APP_MAIN"'
}
